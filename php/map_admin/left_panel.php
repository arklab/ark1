<?php
/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */
/**
* map_admin/left_panel.php
*
* the left panel for the map_admin view
*
* PHP versions 4 and 5
*
* LICENSE:
*    ARK - The Archaeological Recording Kit.
*    An open-source framework for displaying and working with archaeological data
*    Copyright (C) 2022 Museum of London Archaeology.
*    This program is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*    This program is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*    You should have received a copy of the GNU General Public License
*    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*
* @category   map
* @package    ark
* @author     Stuart Eve <stuarteve@lparchaeology.com>
* @copyright  1999-2011 L - P : Partnership Ltd.
* @copyright  2011-2022 L - P : Heritage LLP.
* @copyright  2022 Museum of London Archaeology
* @link       http://ark1.arklab.uk/
* @link       http://gitlab.com/arklab/ark1
* @since      File available since Release 0.8
*/

?>
<h1><?=getMarkup('cor_tbl_markup', $lang, 'mapadmin')?></h1>
<div id="legend_panel"><label>A map legend will be automatically inserted here</label></div>
