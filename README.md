# ARK v1

## WARNING AND DISCLAIMER

DO NOT USE. ARK v1 is obsolete and unsupported. It uses insecure and unsupported versions of PHP and other libraries. Please do not use ARK v1 for anything other than academic study of the history of digital field recording databases.

## About ARK

ARK 1 (The Archaeological Recording Kit v1) was a web-based ‘toolkit’ for the collection, storage and dissemination of archaeological data.  It included data-editing, data-creation, data-viewing and data-sharing tools, all of which were delivered using a web-based front-end.

It was designed to be adaptable to any digital or paper-based recording system, so did not dictate what or how the archaeologist recorded at a given site. Rather it provided a framework, an interface and a set of pre-fabricated digital tools for archaeological recording and data dissemination according to the unique needs of any given project.

Based on industry standard data technologies (Apache/MySQL/PHP), ARK was completely opensource and standards-compliant.

Development of ARK was led and supported by L - P : Archaeology (http://www.lparchaeology.com/). L - P : Archaeology is now part of Museum of London Archaeology (MOLA) (http://www.mola.org.uk/). The ARK Project continues at MOLA and can be found at http://www.arklab.uk and http://gitlab.com/arklab

## ARK 1.1.2 Stable Release

This branch is a copy of the final full release tarball of ARK v1.1.2 that could be downloaded from the ARK1 website and Sourceforge repository, but with updated copyright statements and whitespace fixes.  Please visit https://gitlab.com/arklab/ark1/-/wikis/home for more information and installation instructions.
